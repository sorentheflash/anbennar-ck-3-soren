﻿morbani = {
	color = { 0 0 0 }

	ethos = ethos_bellicose
	heritage = heritage_rohibonic
	language = language_rohibonic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_horse_lords
		tradition_equitable
	}
	
	name_list = name_list_balmirish
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		5 = slavic_blond
		10 = slavic_ginger
		35 = slavic_brown_hair
		40 = slavic_dark_hair
	}
}

arami = {
	color = { 0 0 0 }

	ethos = ethos_spiritual
	heritage = heritage_rohibonic
	language = language_rohibonic_common
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_pastoralists
		tradition_zealous_people
		tradition_battlefield_looters
		tradition_caravaneers
	}
	
	name_list = name_list_crownsman
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		5 = slavic_blond
		10 = slavic_ginger
		35 = slavic_brown_hair
		40 = slavic_dark_hair
	}
}