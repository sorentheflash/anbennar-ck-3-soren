﻿# Here are the titles the baronies get their color from. Uses RGB 255.
d_map_base_color = {
	color = { 84 78 60 }
	can_create = { always = no }
}

d_map_color_red = {
	color = { 180 0 0 }
	can_create = { always = no }
}

d_map_color_dark_red = {
	color = { 60 0 0 }
	can_create = { always = no }
}

d_map_color_green = {
	color = { 0 180 0 }
	can_create = { always = no }
}
d_map_color_blue = {
	color = { 0 0 180 }
	can_create = { always = no }
}
d_map_color_cyan = {
	color = { 0 180 180 }
	can_create = { always = no }
}
d_map_color_yellow = {
	color = { 180 180 0 }
	can_create = { always = no }
}
d_map_color_orange = {
	color = { 255 98 0 }
	can_create = { always = no }
}
d_map_color_pink = {
	color = { 180 0 180 }
	can_create = { always = no }
}